const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const user = require('../controllers/userController');

router.get('/me', asyncWrapper(checkAuth), asyncWrapper(user.getMe));

router.delete('/me', asyncWrapper(checkAuth), asyncWrapper(user.delete));

router.patch('/me', asyncWrapper(checkAuth), asyncWrapper(user.changePassword));

module.exports = router;
