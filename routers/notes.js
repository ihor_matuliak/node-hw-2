const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const notes = require('../controllers/notesController');

router.get('/', asyncWrapper(checkAuth), asyncWrapper(notes.getNotes));

router.post('/', asyncWrapper(checkAuth), asyncWrapper(notes.addNote));

router.get('/:id', asyncWrapper(checkAuth), asyncWrapper(notes.getNoteById));

router.put('/:id', asyncWrapper(checkAuth), asyncWrapper(notes.updateNoteById));

router.patch('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(notes.changeCompleteById));

router.delete('/:id',
    asyncWrapper(checkAuth),
    asyncWrapper(notes.deleteNoteById));

module.exports = router;
