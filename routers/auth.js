const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {validateRegister} = require('../middlewares/validationMiddleware');
const auth = require('../controllers/authController');

router.post('/login', asyncWrapper(auth.login));
router.post('/register',
    asyncWrapper(validateRegister),
    asyncWrapper(auth.registration));

module.exports = router;
