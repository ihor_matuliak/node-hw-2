const User = require('../models/user');
const bcrypt = require('bcrypt');

module.exports.getMe = async (req, res) => {
  const user = await User.findOne({_id: req.user._id}, {password: 0, __v: 0});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  res.status(200).json({user});
};

module.exports.delete = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User not found'});
  }

  await User.deleteOne({_id: user._id});

  res.status(200).json({message: 'User was deleted successfully'});
};

module.exports.changePassword = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const {oldPassword, newPassword} = req.body;

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Passwords do not match'});
  }

  await User.updateOne(user, {
    password: await bcrypt.hash(newPassword, 10),
  });

  res.status(200).json({message: 'Password was change'});
};
