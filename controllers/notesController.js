const User = require('../models/user');
const Note = require('../models/note');

module.exports.getNotes = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const {offset, limit} = req.body;
  const notes = await Note.find(
      {
        userId: user._id,
      },
      {__v: 0},
  )
      .skip(offset)
      .limit(limit);

  res.status(200).json({notes});
};

module.exports.addNote = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'Please log in'});
  }

  const note = new Note({
    userId: user._id,
    text: req.body.text,
  });

  await note.save();

  res.status(200).json({message: 'Note was create'});
};

module.exports.getNoteById = async (req, res) => {
  const id = req.params.id;

  try {
    const note = await Note.findById(id, {__v: 0});
    res.status(200).json({note});
  } catch (e) {
    return res.status(400).json({message: 'Note not found'});
  }
};

module.exports.updateNoteById = async (req, res) => {
  const id = req.params.id;
  const text = req.body.text;

  try {
    const note = await Note.findById(id, {__v: 0});
    note.text = text;
    await note.save();
    res.status(200).json({message: 'Note was change'});
  } catch (e) {
    return res.status(400).json({message: 'Note not found'});
  }
};

module.exports.changeCompleteById = async (req, res) => {
  const id = req.params.id;

  try {
    const note = await Note.findById(id);
    note.completed = note.completed === true ? false : true;
    await note.save();
    res.status(200).json({message: 'Note was changed'});
  } catch (e) {
    return res.status(400).json({message: 'Note not founded'});
  }
};

module.exports.deleteNoteById = async (req, res) => {
  const id = req.params.id;

  try {
    await Note.deleteOne({_id: id});
    res.status(200).json({message: 'Note was deleted'});
  } catch (e) {
    return res.status(400).json({message: 'Note not founded'});
  }
};
