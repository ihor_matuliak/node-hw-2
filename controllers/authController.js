const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config.js');
const User = require('../models/user');

module.exports.login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});

  if (!user) {
    return res.status(400)
        .json({message: `No user with username '${username}' found!`});
  }

  if (!password) {
    return res.status(400).json({message: `Enter your password`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password`});
  }

  const token = jwt.sign(
      {
        username,
        _id: user._id,
        createDate: user.createDate,
      },
      JWT_SECRET,
  );

  res.status(200).json({
    message: 'Success',
    jwt_token: token,
  });
};

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  if (username && password) {
    let user = await User.findOne({username});
    if (!user) {
      user = new User({
        username,
        password: await bcrypt.hash(password, 10),
      });
      await user.save();

      res.status(200).json({message: 'User created successfully'});
    } else {
      res.status(400).json({message: 'User is already exist'});
    }
  } else {
    res.status(400).json({message: 'Enter username and password'});
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 9),
  });

  await user.save();

  res.status(200).json({message: 'Success'});
};
