const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routers/auth');
const notesRouter = require('./routers/notes');
const userRouter = require('./routers/user');

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://ihor:igor123@cluster0.wzqfq.mongodb.net/user', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
      console.log(`Server is running on PORT:${PORT}`);
    });
  } catch (err) {
    console.log(err);
  }
};

start();
