const {Schema, model} = require('mongoose');

const noteSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  text: {
    type: String,
    trim: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('Note', noteSchema);
