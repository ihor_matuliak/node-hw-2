const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config.js');

module.exports.checkAuth = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401)
        .json({message: 'No Authorization http header found!'});
  }

  const [tokenType, token] = header.split(' ');

  console.log('Token type ', tokenType);

  if (!token) {
    return res.status(401).json({message: 'No JWT http token found!'});
  }

  try {
    req.user = jwt.verify(token, JWT_SECRET);
    next();
  } catch (error) {
    return res.status(400).json({message: 'Token is not correct'});
  }
};
