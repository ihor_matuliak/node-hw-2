const Joi = require('joi');

module.exports.validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(1).max(30).required(),

    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};
